#task 3
#The goal of this task is to have two programs with the fibonacci series
def function3A (max) 
  x = 1
  y = 1

  while x <=max
    yield x 
    x = y
    y = x+y
  end
  
end


def function3B( n )
    return  n  if n <= 1 
    function3B( n - 1 ) + function3B( n - 2 )
end 

function3A(35){|x| puts x}
puts "\n"

puts function3B(32)