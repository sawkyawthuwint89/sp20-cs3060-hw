#task 4
#The goal of task 4 is to add an contructer to a class
class Tree
  attr_accessor :children, :node_name
  
  def initialize(name, children =[])
    @children = children
    @node_name = name
  end

  def visit_all(&block)
    visit &blockk
    children.each {|c| c.visit_all &block}
  end

  def visit(&block)
    block.call self
  end
end

